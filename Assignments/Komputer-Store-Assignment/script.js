const buttonLoan = document.getElementById("button-loan");
const buttonBank = document.getElementById("button-bank");
const buttonWork = document.getElementById("button-work");
const buttonRepay = document.getElementById("button-repay");
const buttonBuy = document.getElementById("button-buy");

const balanceText = document.getElementById("balance");
const loanText = document.getElementById("outstanding-loan");
const payText = document.getElementById("pay");
const message = document.getElementById("message");

const laptopElement = document.getElementById("laptop-list");
const laptopPriceElement = document.getElementById("total");
const laptopDescriptionElement = document.getElementById("description");
const laptopTitleElement = document.getElementById("title");
const laptopImageElement = document.getElementById("image");
const featureElement = document.getElementById("feature-list");

let balance = 0;
let loanAmount = 0;
let pay = 0;
let numberOfLoans = 0;
let laptopPrice = 0;
let laptopArray = [];

const url = "https://noroff-komputer-store-api.herokuapp.com/computers";

const loadData = async (adress) => {
    try {
        const response = await fetch(adress);
        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};

loadData(url)
    .then((data) => (laptopArray = data))
    .then((laptop) => addAllLaptops(laptop));

const getLoanHandler = () => {
    /*
  This Function accepts 0 arguments.
    If various checks are false. This Function will increase the number of loans by 1,
    and update the correct text.

  Args: None.

  return: None.

  Notes: If any check is true, the text will be updated with the correct message, and simply return;
*/
    if (numberOfLoans > 0) {
        makeVisible(message);
        message.innerText = `You allready have 1 loan. You need to repay your loan before taking a new one.`;
        return;
    }

    loanAmount = Number(window.prompt("Enter loan amaount: ", ""));

    if (loanAmount < 1) {
        makeVisible(message);
        message.innerText = `Your loan amount must be greater than 0`;
        return;
    }

    if (loanAmount > balance * 2) {
        makeVisible(message);
        message.innerText = `You cannot get a loan more than double of your bank balance.`;
        return;
    }

    numberOfLoans++;
    checkLoan(numberOfLoans);
    makeVisible(loanText);
    message.innerText = `The loan amout of ${loanAmount}kr was added to your loan.`;
    loanText.innerText = `Outstanding loan: ${loanAmount}kr`;
};

const bankHandler = () => {
    /*
  This Function accepts 0 arguments.
    If various checks are false. This Function will transfer the funds from pay
    to bank and update the correct text.

  Args: None. 

  return: None.

  Notes: If a loan is present, 10% of current income will be added to loan;
*/
    if (pay === 0) {
        makeVisible(message);
        return (message.innerText = `You have no money to transer to your bank`);
    }

    if (numberOfLoans > 0) {
        let deductedAmount = pay * 0.1;
        loanAmount -= deductedAmount;
        pay -= deductedAmount;

        makeVisible(message);
        message.innerText = `10% of your pay was transfered to your outstanding loan amount, the rest was added to your bank`;

        balance += pay;
        pay = 0;
        payText.innerText = `Pay: ${pay}kr`;
        balanceText.innerText = `Balance: ${balance}kr`;
        loanText.innerText = `Outstanding loan: ${loanAmount}kr`;
        return;
    }

    makeVisible(message);
    message.innerText = `Your pay was transfered to your bank`;
    balance += pay;
    pay = 0;
    payText.innerText = `Pay: ${pay}kr`;
    balanceText.innerText = `Balance: ${balance}kr`;
};

const workHandler = () => {
    // Workhandler will simply increase Pay by 100, and update the correct text.
    makeHidden(message);
    pay += 100;
    payText.innerText = `Pay: ${pay}kr`;
};

const repayLoanHandler = () => {
    /*
  This Function accepts 0 arguments.
    If various checks are false. This Function will transfer all funds from pay to loanAmount,
    and update the correct text.

  Args: None.

  return: None.

  Notes: If Pay is >= than loanAmount, the remainding funds will be added to Bank.
*/
    if (numberOfLoans < 1) {
        makeVisible(message);
        message.innerText = `You have no loan to repay.`;
        return;
    }

    if (pay < 1) {
        makeVisible(message);
        message.innerText = `You don't have any money to repay your loan.`;
        return;
    }

    if (pay >= loanAmount) {
        let remainder = pay - loanAmount;
        loanAmount = 0;
        pay = 0;
        numberOfLoans--;
        balance += remainder;

        checkLoan(numberOfLoans);

        makeVisible(message);
        makeHidden(loanText);
        message.innerText = `Your loan has been repaid, and the remaining pay has been added to your bank.`;

        payText.innerText = `Pay: ${pay}kr`;
        balanceText.innerText = `Balance: ${balance}kr`;
        loanText.innerText = `Outstanding loan: ${loanAmount}kr`;
        return;
    }

    makeVisible(message);
    message.innerText = `Your loan has been reduced by ${pay}kr`;

    loanAmount -= pay;
    pay = 0;

    payText.innerText = `Pay: ${pay}kr`;
    loanText.innerText = `Outstanding loan: ${loanAmount}kr`;
};

const laptopChangeHandler = (event) => {
    const selectedLaptop = laptopArray[event.target.selectedIndex];
    laptopPrice = selectedLaptop.price;
    laptopPriceElement.innerText = `${selectedLaptop.price} kr`;
    laptopDescriptionElement.innerText = selectedLaptop.description;
    laptopTitleElement.innerText = selectedLaptop.title;
    laptopImageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedLaptop.image}`;

    // This while loop remove all "li"-elements from the ul. 
    // Without this, whenever the laptopChangeHandler event fires, the new li items will append to the old list.
    while (featureElement.firstChild) {
        featureElement.removeChild(featureElement.lastChild);
    }
    populateList(event);
};

const populateList = (event) => {
    /*
  This Function accepts 0 arguments.
    This Function uses a double loop at the specific index to create the feature list

  Args: None.

  return: All <li></li> items

  Notes: Honestly I'm 100% certain there are easier ways to do this, but I spend several hours
         knocking my head against the wall trying to get the very last piece of code to work and
         I'm just happy it works. Although it probably definitely is spagetti code...
*/
    laptopArray.map((element) => element.specs)[event.target.selectedIndex]
    .map((listItem) => {
            let childNode = document.createElement("li");
            childNode.innerHTML = listItem;
            featureElement.appendChild(childNode);
        });
};

const buyHandler = () => {
    /*
  This Function accepts 0 arguments.
    This Function will attempt to buy a laptop, if funds are sufficient.

  Args: None.

  return: None.

  Notes: If funds are not sufficient, the Function will update the correct text and simply return;
*/
    if (balance >= laptopPrice) {
        balance -= laptopPrice;
        makeVisible(message);
        message.innerText = `Congratulations! You just bought a brand new computer`;
        balanceText.innerText = `Balance: ${balance}kr`;
        return;
    }
    if (balance < laptopPrice) {
        makeVisible(message);
        message.innerText = `Sorry! You don't have sufficient funds to buy that laptop`;
        balanceText.innerText = `Balance: ${balance}kr`;
        return;
    }
};

const addAllLaptops = (laptops) => {
    laptops.map((element) => addSingleLaptop(element));

    // Initial values for the Laptop feature list
    laptops[0].specs.map((listItem) => {
        let childNode = document.createElement("li");
        childNode.innerHTML = listItem;
        featureElement.appendChild(childNode);
    });

    // Initial values for the Laptop
    laptopPrice = laptops[0].price;
    laptopPriceElement.innerText = `${laptops[0].price} kr`;
    laptopDescriptionElement.innerText = laptops[0].description;
    laptopTitleElement.innerText = laptops[0].title;
    laptopImageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${laptops[0].image}`;
};

const addSingleLaptop = (laptop) => {
    const createdElement = document.createElement("option");
    createdElement.value = laptop.id;
    createdElement.appendChild(document.createTextNode(laptop.title));
    laptopElement.appendChild(createdElement);
};

const checkLoan = (loanNumber) => {
    // Function to remove or include the "Loan-Button" to the document flow
    loanNumber > 0
        ? (buttonRepay.style.display = "inline-block")
        : (buttonRepay.style.display = "none");
};

const makeHidden = (domElement) => {
    domElement.classList.remove("visible");
    domElement.classList.add("hidden");
};

const makeVisible = (domElement) => {
    domElement.classList.add("visible");
    domElement.classList.remove("hidden");
};

buttonLoan.addEventListener("click", getLoanHandler);
buttonBank.addEventListener("click", bankHandler);
buttonWork.addEventListener("click", workHandler);
buttonRepay.addEventListener("click", repayLoanHandler);
buttonBuy.addEventListener("click", buyHandler);

laptopElement.addEventListener("change", laptopChangeHandler);