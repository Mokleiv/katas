/*
  This Function counts how many times a boomerang can be defined as
  a sub-array of length 3, with the first and last digits being the same
  and the middle digit being different.
*/

// const numberOfBoomerangs = (array) => {
//     let count = 0;
//     for (let i = 0; i < array.length; i++) {
//         if (array[i] === array[i + 2] && array[i] !== array[i + 1]) {
//             count++;
//         }
//     }
//     return count;
// };

const numberOfBoomerangs = (array) => array.filter((el, index, arr) => (arr[index] === arr[index + 2] && arr[index] !== arr[index + 1])).length;

console.log(numberOfBoomerangs([9, 5, 9, 5, 1, 1, 1]));
console.log(numberOfBoomerangs([5, 6, 6, 7, 6, 3, 9]));
console.log(numberOfBoomerangs([4, 4, 4, 9, 9, 9, 9]));
console.log(numberOfBoomerangs([1, 7, 1, 7, 1, 7, 1]));
console.log(numberOfBoomerangs([5, 5, 5]));