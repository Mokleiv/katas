/*
  This Function checks wheter a year is a leap year or not and
  returns true or false.
*/

const leapYear = (year) => {
  if((year % 4 === 0) && (year % 100 !== 0) || (year % 400 === 0)) {
    return true;
  } else {
    return false;
  }
}

console.log(leapYear(1990));
console.log(leapYear(1924));
console.log(leapYear(2021));