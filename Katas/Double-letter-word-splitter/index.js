/*
  This function receives a word of type string,
  splits the word where any double letter is found,
  and returns an array of the split word.
  If no repeated letters are found, it will return an empty array.
*/

const splitOnDoubleLetter = (word) => {
    let wordArray = [...word.toLowerCase()];    // -> ["h", "a", "p", "p", "y"]
    for (let i = 0; i < wordArray.length; i++) {
        if (wordArray[i] === wordArray[i - 1]) {
            wordArray.splice(i, 0, ".");        // -> ["h", "a", "p", ".", "p", "y"]
        }
    }
    return wordArray.join("").split(".").length > 1
        ? wordArray.join("").split(".")
        : [];
};

console.log(splitOnDoubleLetter("happy"));
console.log(splitOnDoubleLetter("Aardvark"));
console.log(splitOnDoubleLetter("mississippi"));
console.log(splitOnDoubleLetter("easy"));