/*
  This Function returns the difference between the sum of the squares
  of n natural numbers and the square of the sum.
*/

const sumSquareDifference = (num) => {
    // Sum of Squares of n Natural Numbers Formula = ((n(n+1)(2n+1))/6)
    const sumSquared = (num * (num + 1) * (2 * num + 1)) / 6;

    // Sum of n Natural Numbers Formula = ((n(n+1))/2)
    const squaredSum = ((num * (num + 1)) / 2) ** 2;

    return squaredSum - sumSquared;
};

console.log(sumSquareDifference(10));