/*
  This Function decrypts a Caesar Cipher by shifting each letter by a number(rotationFactor) of letters.
  If the shift takes you past the end of the alphabet, just rotate back to the front of the alphabet.
*/

const caesarCipher = (str, rotationFactor) => {
    const upperAsciiMax = 90 - rotationFactor;
    const lowerAsciiMax = 122 - rotationFactor;
    
    let alfabetLength = 26;
    let deciphered = "";

    for (let i = 0; i < str.length; i++) {
        let ascii = str[i].charCodeAt();

        if (ascii >= 65 && ascii <= upperAsciiMax) {
            deciphered += String.fromCharCode(ascii + rotationFactor);
        } 
        else if (ascii >= upperAsciiMax + 1 && ascii <= 90) {
            deciphered += String.fromCharCode(ascii - alfabetLength + rotationFactor);
        } 
        else if (ascii >= 97 && ascii <= lowerAsciiMax) {
            deciphered += String.fromCharCode(ascii + rotationFactor);
        } 
        else if (ascii >= lowerAsciiMax + 1 && ascii <= 122) {
            deciphered += String.fromCharCode(ascii - alfabetLength + rotationFactor);
        } 
        else {
            deciphered += str[i];
        }
    }
    return deciphered;
};

console.log(caesarCipher("Always-Look-on-the-Bright-Side-of-Life", 5));
console.log(caesarCipher("A friend in need is a friend indeed", 20));