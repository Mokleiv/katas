/*
  This function accepts a string of lowercase characters in range ascii[["a".."z"]].
  It reduced the string by selecting a pair of adjacent letters that match and deletes them
  until the string is as short as possible.
  If the final result is an emptry string, "Empty String" is returned.
*/

const superReducedString = (str) => {
    let strArray = [...str];
    for (let i = 0; i < strArray.length; i++) {
        if (strArray[i] === strArray[i + 1]) {
            strArray.splice(i, 2);
            i = -1;
        }
    }
    return strArray.length === 0 ? "Empty String" : strArray.join("");
};

console.log(superReducedString("cccxllyyy"));
console.log(superReducedString("aa"));
console.log(superReducedString("baab"));
console.log(superReducedString("fghiiijkllmnnno"));
console.log(superReducedString("chklssstt"));