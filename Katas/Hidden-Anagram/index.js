/*
  This Function that takes two strings. The first string contains a sentence 
  containing the letters of the second string in a consecutive sequence but in a different order. 
  The hidden anagram must contain all the letters, including duplicates,
  from the second string in any order and must not contain any other alphabetic characters.
*/

const hiddenAnagram = (str1, str2) => {
    // anoldwestactionheroactor
    str1 = str1.toLowerCase().replace(/[^a-z]/g, "");
    // acdeilnoosttw
    str2 = [...str2.toLowerCase().replace(/[^a-z]/g, "")].sort().join("");


    for (i = 0; i <= str1.length - str2.length; i++) {
        const result = str1.substr(i, str2.length);

        if ([...result].sort().join("") === str2) {
            return result;
        }

    }
    return "noutfond";
};

console.log(hiddenAnagram("An old west action hero actor", "Clint Eastwood"));
console.log(hiddenAnagram("Mr. Mojo Rising could be a song title", "Jim Morrison"));
console.log(hiddenAnagram("Banana? margaritas", "ANAGRAM"));
console.log(hiddenAnagram("D e b90it->?$ (c)a r...d,,#~", "bad credit"));
console.log(hiddenAnagram("Bright is the moon", "Bongo mirth"));
