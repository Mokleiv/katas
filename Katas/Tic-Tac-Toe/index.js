/*
  This Function determines the winner of a game of Tic Tac Toe.
  The Function return whether the game is a win for "X", "O", or a "Draw",
  where "X" and "O" represent themselves on the matrix, and "E" represents an empty spot.
*/

const ticTacToeChecker = (matrix) => {
    const xRegex = /XXX/g;
    const oRegex = /OOO/g;

    const horizontal = matrix[0].map((el, index) =>
        matrix.map((cur) => cur[index]).join("")
    );
    const vertical = matrix.map((el) => el.join(""));

    let firstDiagonal = "";
    for (let i = 0; i < 3; i++) {
        firstDiagonal += matrix[i][i];
    }

    let j = 0;
    let secondDiagonal = "";
    for (let i = 2; i > -1; i--) {
        secondDiagonal += matrix[i][j];
        j++;
    }

    if (horizontal.some((el) => xRegex.test(el)) || vertical.some((el) => xRegex.test(el))) { return "X"; }

    if (horizontal.some((el) => oRegex.test(el)) || vertical.some((el) => oRegex.test(el))) { return "O"; }

    if (xRegex.test(firstDiagonal) || xRegex.test(secondDiagonal)) { return "X;"; }

    if (oRegex.test(firstDiagonal) || oRegex.test(secondDiagonal)) { return "O;"; }

    return "Draw";
};

console.log(
    ticTacToeChecker([
        ["X", "O", "X"],
        ["O", "X", "O"],
        ["O", "X", "X"],
    ])
);

console.log(
    ticTacToeChecker([
        ["O", "O", "O"],
        ["O", "X", "X"],
        ["E", "X", "X"],
    ])
);

console.log(
    ticTacToeChecker([
        ["X", "X", "O"],
        ["O", "O", "X"],
        ["X", "X", "O"],
    ])
);