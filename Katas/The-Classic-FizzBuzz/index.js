/*
  This function receives a number n and prints each number from 1 to n, to the console.
  For each multiple of 3 it instead prints "Fizz".
  For each multiple of 5 it instead prints "Buzz".
  For each multiple of both 3 and 5 it prints "FizzBuzz".
*/

const multipleOfThreeAndFive = (number) => {
    for(let i = 1; i <= number; i++) {
        if(i % 3 === 0 && i % 5 === 0) {
            console.log("FizzBuzz");
        }
        else if(i % 3 === 0) {
            console.log("Fizz");
        }
        else if(i % 5 === 0) {
            console.log("Buzz");
        } else {
            console.log(i);
        }
    }
}

multipleOfThreeAndFive(30);