/*
  A Faro Shuffle is a controlled method for shuffling playing cards.
  It is performed by splitting the deck into two equal halves and interleaving them together perfectly,
  with the condition that the top card of the deck remains in place.
*/

const shuffleCount = (num) => {
    const deck = createDeck(num);
    let shuffledDeck = faroShuffle(deck);
    let count = 1;

    while (!equalArrayCheck(deck, shuffledDeck)) {
        shuffledDeck = faroShuffle(shuffledDeck);
        count++;
    }
    return count;
};

const createDeck = (size) => {
    let deck = [];
    for (let i = 1; i <= size; i++) {
        deck.push(i);
    }
    return deck;
};

const faroShuffle = (deck) => {
    let firstHalf = deck.slice(0, deck.length / 2); // [1, 2, 3, 4]
    let secondHalf = deck.slice(deck.length / 2);   // [5, 6, 7, 8]
    let shuffledDeck = [];

    for (let i = 0; i < firstHalf.length; i++) {
        shuffledDeck.push(firstHalf[i], secondHalf[i]); // [1, 5, 2, 6, 3, 7, 4, 8]
    }

    return shuffledDeck;
};

const equalArrayCheck = (array1, array2) => {
    if (array1.length !== array2.length) {
        return false;
    }

    // Check if all items exist and are in the same order
    for (let i = 0; i < array1.length; i++) {
        if (array1[i] !== array2[i]) {
            return false;
        }
    }

    return true;
};

console.log(shuffleCount(8));
console.log(shuffleCount(14));
console.log(shuffleCount(52));