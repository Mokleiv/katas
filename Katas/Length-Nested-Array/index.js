/*
  This function receives an array with n nested arrays.
  Using the .flat() method with Infinity as depth, it "flattens" the array,
  and returns the number of elements in the array.
*/

const lengthOfNestedArray = (array) => array.flat(Infinity).length;

console.log(lengthOfNestedArray([1, [2, 3]]));
console.log(lengthOfNestedArray([1, [2, [3, 4]]]));
console.log(lengthOfNestedArray([1, [2, [3, [4, [5, 6]]]]]));
console.log(lengthOfNestedArray([1, [2], 1, [2], 1]));
console.log(lengthOfNestedArray([]));