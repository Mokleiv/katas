/*
  This function receives the amount of two-points and three-pointers
  scored in a basketball game and
  calculates the total points scored.
*/

const countPoints = (twoPoint, threePoint) => ((twoPoint * 2) + (threePoint * 3));

console.log(countPoints(0, 1));
console.log(countPoints(1, 1));
console.log(countPoints(38, 8));
console.log(countPoints(0, 0));