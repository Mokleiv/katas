/*
  This Function takes in a String and returns it with the correct case and standards.

  - The words and, the, of and in should be lowercase.
  - All other words should have the first character as uppercase and the rest lowercase.
  - All commas must always be followed by a single space.
  - All titles must end with a period.
  - Punctuation and spaces must remain in their original positions.
  - Hyphenated words are considered separate words.
*/

const characterTitleFixer = (str) => {
    let strArray = [...str.toLowerCase()];

    // Add "." to the end
    if (!(strArray[strArray.length - 1] === ".")) {
        strArray.push(".");
    }

    // Check for "," and add space if needed. Also checks for "-"
    for (let i = 0; i < strArray.length; i++) {
        if (strArray[i] === "," && /[a-zA-z]/g.test(strArray[i + 1])) {
            strArray.splice(i + 1, 0, " ");
            i++;
        }
        if(strArray[i] === "-") {
            strArray[i + 1] = strArray[i + 1].toUpperCase();
        } 
    }

    // Give all word capital letter unless it's "and", "the", "of" or "in".
    return strArray
        .join("")
        .split(" ")
        .map((word) =>
            word === "and" || word === "the" || word === "of" || word === "in"
                ? word
                : word.charAt(0).toUpperCase() + word.slice(1)
        )
        .join(" ");
};

console.log(characterTitleFixer("jOn SnoW, kINg IN thE noRth"));
console.log(characterTitleFixer("sansa stark,lady of winterfell."));
console.log(characterTitleFixer("TYRION LANNISTER, HAND OF THE QUEEN."));
console.log(characterTitleFixer("TYRION-LANNISTER, HAND OF THE QUEEN."));